deploy-influxdb2:
	helm upgrade --install influxdb influxdata/influxdb2

deploy-telegraf-ds:
	helm upgrade --install telegraf-ds -f telegraf-ds.yaml influxdata/telegraf-ds

deploy-telegraf-kubernetes:
	helm upgrade --install telegraf-k8s -f telegraf-k8s.yaml influxdata/telegraf
